package Refelction.API.Grundlagen;

import java.lang.reflect.InvocationTargetException;
import java.util.Scanner;

interface HelloMeth
 {
   public void hello();
 }
 
 class CA implements HelloMeth
 {
   public void hello()
   {
     System.out.println("hello CA");
   }
 }
 
 class CB  implements HelloMeth
 {
   public void hello()
   {
     System.out.println("hello CB");
   }
 }
 
 class CC  {
   public void hello()
   {
     System.out.println("hello CC");
   }
 }
 
 class CD
 {
   public void hallo()
   {
     System.out.println("hallo CD");
   }
 }
 
 /**
  * Hier ein einfaches Beispiel f�r das  dynamische Laden und Instanzieren von Klassen.
  * 
  * HelloMeth deklariert die Methode hello, die von den Klassen CA und CB implementiert wird. 
  * CC besitzt ebenfalls die  Methode hello, allerdings ohne das Interface HelloMeth zu implementieren. 
  * CD schlie�lich implementiert nicht hello, sondern hallo.
  *
  * Die Hauptklasse liest zun�chst einen Klassennamen von der Standardeingabe ein.
  * Mit der Klassenmethode forName der Klasse Class wird dann ein Klassenobjekt zu einer Klasse dieses Namens beschafft.
  * Das widerum verwendet, um mit der Methode getConstructor().newInstance()
  * der Klasse Class ein neues Objekt zu erzeugen. 
  * Dieses Objekt wird schlie�lich in das Interface HelloMeth konvertiert und dessen Methode hello aufgerufen.
  * 
  * Damit ist das Programm in der Lage, die beiden Klassen CA und CB ordnungsgem�� zu
  * instanzieren und ihre Methode hello aufzurufen. Bei CC und CD gibt es eine Ausnahme 
  * des Typs ClassCastException, weil diese Klassen nicht das Interface HelloMeth implementieren.
  * 
  *  Alle anderen Klassennamen werden mit der Ausnahme ClassNotFoundException quittiert.
  * 
  * 
  * @author Susi
  *
  */
 
class ReflectionDemo_App{
 
   public static void main(String[] args) throws IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException
   {
	   var buf ="";
  // alt   var reader =  new InputStreamReader( new DataInputStream(System.in));
  // alt   var in = new BufferedReader(reader);
	  var scanner  = new Scanner(System.in);
     while (true) {
     try {
    	 //scanner.nextLine();
         System.out.print("Klassenname oder ende eingeben: ");
         
         buf = ((Scanner) scanner).nextLine();
         if (buf.equals("ende")) {
           break;
         }
         Class<?> c = Class.forName(buf);
         //Object o = c.newInstance();  //Deprecated
         Object o = c.getConstructor().newInstance();
         ((HelloMeth)o).hello();
       }catch (ClassNotFoundException e) {
         System.out.println("Klasse nicht gefunden");
       } catch (ClassCastException e) {
         System.out.println(e.toString());
       } catch (InstantiationException e) {
         System.out.println(e.toString());
       } catch (IllegalAccessException e) {
         System.out.println(e.toString());
       }
     }
   }
 }
