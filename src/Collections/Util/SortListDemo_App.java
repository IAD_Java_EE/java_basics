package Collections.Util;

import java.util.ArrayList;
import java.util.Collections;



public class SortListDemo_App {

	public static void main(String[] args) {
		
		var s = new ArrayList<String>();
		s.add("Zitrone");
		s.add("Kirsche");
		s.add("Ananas");
		s.add("Apfel");
		s.add("Grapefruit");
		s.add("Banane");
		
		//Unsortierte Ausgabe
		var it = s.iterator();
		while (it.hasNext()) {
			System.out.println((String) it.next());
		}
		
		
		System.out.println("---");
		// Sortierte Ausgabe
		Collections.sort(s);
		it = s.iterator();
		while (it.hasNext()) {
			System.out.println((String) it.next());
		   }
		
	}

}
