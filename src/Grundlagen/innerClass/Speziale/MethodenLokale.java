package Grundlagen.innerClass.Speziale;


public class MethodenLokale {

	@SuppressWarnings("unused")
	public void doFoo() {
		
		int counter = 42;
		final int CONSTANT = 100;

		/* keine Sichtbarkeit erlaubt (!) sonst Compiler Fehler */
		class MethodeLocalInnerClass {
			public void display() {
				//System.out.println("Z�hlerstand " + counter);
				System.out.println("Konstante " + CONSTANT);
			}
		}

		++counter; // Durch diese Zeile wird Zugriff auf conter in display verhindert! 
				// effectivelky final
		//https://stackoverflow.com/questions/20938095/difference-between-final-and-effectively-final
		new MethodeLocalInnerClass().display();
	}

}
