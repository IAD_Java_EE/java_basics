package Grundlagen.innerClass.Speziale;

/*public class AnonymeInnerClass {
	//  Klassen f�r eine einmalige Aufgabe anwendbar
	//SAM  Single Abstract Method - F�r Lambdas wichtig
	
	// Sind ausschlie�lich zum �berschreiben der Methoden aus der Basisklasse gedacht!
	
	final Runnable newRunnable = new Runnable() {
		
		@Override
		public void run() {
			System.out.println("Es l�uft doch");
			
		}
	}; // Semikolon ist wichtig!!
}*/
public abstract class AnonymeInnerClass{
	
	public abstract void run() ;
	

}
