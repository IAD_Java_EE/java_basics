package Grundlagen.innerClass.Speziale;

public class InnerClassSpeziale_App {

	public static void main(String[] args) {
		new InnerClassSpeziale_App().Test();
		
		

	}
	
	public void Test() {
		
		final AnonymeInnerClass runner = new AnonymeInnerClass() {
			
			@Override
			public void run() {
				System.out.println("Und es l�uft");
				
			}
		};
		runner.run();
	}

}
