package Grundlagen.innerClass;

public class Calculator_App {

	public interface IBerechnen {
		public int berechne(final int op1, final int op2);

		public String getName();
	}

	// Nur zur Demon! Vererbung
	private static abstract class AllgemeineBerechnung implements IBerechnen {
		public String getName() {
			return getClass().getSimpleName();
		}
	}

	// F�r alle Klassen zugreifbar
	public static final class Add extends AllgemeineBerechnung {
		public int berechne(final int op1, final int op2) {
			return op1 + op2;
		}
	}

	// Nur innerhalb des Packages und abgeleiteter Klassen zugreifbar
	protected static final class Sub extends AllgemeineBerechnung {
		public int berechne(final int op1, final int op2) {
			return op1 - op2;
		}
	}

	// Eher selten sinnvoll, nur innerhalb dieser Klasse zugreifbar
	private static class Modulo extends AllgemeineBerechnung {
		public int berechne(final int op1, final int op2) {
			return op1 % op2;
		}
	}

	public static void main(String[] args) {
		final IBerechnen[] berechnungen = { new Add(), new Sub(), new Modulo() };

		for (final IBerechnen rechne : berechnungen) {
			System.out.println("7 " + rechne.getName() + " 2 = " + rechne.berechne(7, 2));
		}
	}

}
