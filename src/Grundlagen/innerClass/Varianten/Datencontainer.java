package Grundlagen.innerClass.Varianten;

public final class Datencontainer {
	public static final class Kompass{
		private final int nord, sued,ost,west;
		
		public Kompass(final int n,
						final int s,
						final int o,
						final int w) {
			this.nord=n;
			this.sued= s;
			this.ost =o ;
			this.west = w;
		}
		
		public final int getNord() {return this.nord;}
		public final int getSued() {return this.sued;}
		public final int getOst() {return this.ost;}
		public final int getWest() {return this.west;}
	}
}
