package Grundlagen.innerClass.Varianten;

public class OuterClass{
	public class InnerClass{
		public void foo() {
			System.out.println("foo wirkt");
		}
		protected void fooProtect() {
			System.out.println("foo wirkt auch protected");
		}
	}
	
	public static final  class InnerStaticClass{
		public static void bar() {
			System.out.println("bar wirkt");
		}
	}

	public Object InnerClass;
}