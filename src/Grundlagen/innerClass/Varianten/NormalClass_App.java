package Grundlagen.innerClass.Varianten;

public class NormalClass_App {

	public static void main(String[] args) {

		final OuterClass.InnerClass inner = new OuterClass().new InnerClass();
		inner.foo();
		
		final OuterClass outer= new OuterClass();
		final OuterClass.InnerClass inner2 = outer.new InnerClass();
		inner2.foo();
		
		OuterClass.InnerStaticClass.bar();
		
		OuterClassPackage.InnerStaticClass.bar();
		
		final SpecialOuterClass fouter= new SpecialOuterClass();
		fouter.fooBasis();
		
		
		@SuppressWarnings("unused")
		final Datencontainer.Kompass k = new Datencontainer.Kompass(0, 180, 90, 270);
	}

}
