package Grundlagen.innerClass.Varianten;

class OuterClassPackage {
	
	public class InnerClass {
		public void foo() {
			System.out.println("foo wirkt");
		}
	}

	public static final class InnerStaticClass {
		public static void bar() {
			System.out.println("bar wirkt");
		}
	}
}
