package Grundlagen.innerClass.Varianten;

public class SpecialOuterClass extends OuterClass{

	public void fooBasis() {
		final OuterClass.InnerClass inner =  new OuterClass() .new InnerClass();
		inner.fooProtect();
	}
}
