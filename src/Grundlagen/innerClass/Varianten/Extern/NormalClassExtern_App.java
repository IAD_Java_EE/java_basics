package Grundlagen.innerClass.Varianten.Extern;

import Grundlagen.innerClass.Varianten.OuterClass;


public class NormalClassExtern_App {

	public static void main(String[] args) {
		final OuterClass.InnerClass inner = new OuterClass().new InnerClass();
		inner.foo();
		
		final OuterClass outer= new OuterClass();
		final OuterClass.InnerClass inner2 = outer.new InnerClass();
		inner2.foo();
		
		OuterClass.InnerStaticClass.bar();
		
		//Nicht sichtbar
		//OuterClassPackage.InnerStaticClass.bar();

	}

}
