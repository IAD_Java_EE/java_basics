package Grundlagen.ClassObject;

import java.util.Date;

public class Class_To_String {

	 public static void main(final String[] args)
	    {
	        final Object obj1 = new Object();
	        final Person obj2 = new Person("Toni", new Date(), "Erfurt");
	        final Object[] objectArray = new Object[] { obj1, obj2 };

	        System.out.println("Object: " + obj1);
	        System.out.println("Person: " + obj2);
	        System.out.println("Object[]: " + objectArray);
	        
	        System.out.println("Das ist aber "+ null + " komisch!"); // valueof
	        System.out.println(obj2);
	    }
	    
	    private Class_To_String ()
	    {
	    }
	    
	    
	    //
	    

}
