package Grundlagen.ClassObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

public class Person_App {

	public static void main(String[] args) {
		
		//System.out.println(new Person("Toni",new Date() ,"Erfurt").toString());
		//new Person(null,new Date() ,"Erfurt").toString();
		new PersonOld(null,new Date(), "Jena", "  " , " ");
		//System.out.println(new Mitarbeiter("Toni").toString());
		
		//https://stackoverflow.com/questions/858572/how-to-make-a-new-list-in-java
		
		//https://wiki.eclipse.org/FAQ_How_do_I_run_Eclipse%3F
		
		Vector<Integer> list= new Vector<Integer>();
		list.addElement(1);
		list.addElement(2);

		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.elementAt(i));
		}
		System.out.printf("%22s\n","1960");
		List<Integer> list2= new ArrayList<Integer>();
		list2.add(1);
		list2.add(2);

		for (Iterator<Integer> itr= list2.iterator(); itr.hasNext();) {
			System.out.println(itr.next());
		}

		System.out.printf("%22s\n","1998");
		List<Integer> list3= new ArrayList<Integer>(Arrays.asList(1, 2));
		for(int item : list3) {
			System.out.println(item);
		}
		
		System.out.printf("%22s\n","2004 Java 5");
		List<Integer> list4= new ArrayList<>(Arrays.asList(1, 2));
		for(int item : list4) {
			System.out.println(item);
		}
		
		System.out.printf("%22s\n","2011 Java 7");
		List<Integer> list5= new ArrayList<>(Arrays.asList(1, 2));
		for(int item : list5) {
			System.out.println(item);
		}
		
		System.out.printf("%22s\n","2014 Java 8");
		List<Integer> list6= new ArrayList<>(List.of(1, 2));

		list6.forEach(System.out::println);

		System.out.printf("%22s\n","2018");
		var list7 = List.of(1, 2);

		list7.forEach (System.out::println);

	}

}
