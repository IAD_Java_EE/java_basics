package Grundlagen.ClassObject;

public final class Mitarbeiter {
	private final String _name;

	public Mitarbeiter(final String name) {
		this._name = name;
	}

	public String get_name() {
		return _name;
	}
	
	
	@Override
	public String toString() {
		
		return "Toni ist der beste";
	}

	@SuppressWarnings("unused")
	private String printf(String string, String _name2) {
		
		return "Mitarbeiter ist %s "+this._name;
	}

}
