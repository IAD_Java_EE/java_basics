package Grundlagen.ClassObject;

import java.util.Arrays;
import java.util.Date;
import java.util.Objects;



public class Array_to_String {

	 public static void main(final String[] args)
	    {
		  final Person pers1 = new Person("Toni", new Date(), "Erfurt");
	        final Person pers2 = new Person("Susi", new Date(), "Jena");
	        final Object[] persons = new Object[] { pers1, pers2 };

	        System.out.println("Object[]: " + Arrays.asList(persons));   // JDK 1.4
	        System.out.println("Object[]: " + Arrays.toString(persons)); // JDK 5.0
	    }

	    private Array_to_String()
	    {
	    }

	    private static class Person
	    {
	        private final String name;
	        private final Date   birthday;
	        private final String city;

	        public Person(final String name, final Date birthday, final String city)
	        {
	        	// Hilfsklasse Objects neu in JDK 7
	        	Objects.requireNonNull(name, "parameter 'name' darf nicht leer sein!");
	        	Objects.requireNonNull(city, "parameter 'city' darf nicht leer sein!");
	        	Objects.requireNonNull(city, "parameter 'city' darf nicht leer sein!");
	            
	            this.name = name;
	            this.birthday = birthday;
	            this.city = city;
	        }

	        public final String getName()
	        {
	            return name;
	        }

	        public final Date getBirthday()
	        {
	            return birthday;
	        }

	        public final String getCity()
	        {
	            return city;
	        }

	        @Override
	        public String toString()
	        {
	            return getClass().getSimpleName() + ": " + 
	                   "Name      ='" + getName() + "' " +		 
	                   "Geburtstag ='" + getBirthday() + "' " +  
	            	   "Stadt      ='" + getCity() + "'";                          
	        }
	    }

}
