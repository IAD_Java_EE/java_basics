package Grundlagen.ClassObject;

import java.util.Date;
import java.util.Objects;

public final class Person {
	 private final String _name;
	 private final Date _birthday;
	 private final String _city;

	    public Person(final String name, final Date birthday, final String city)
	    {
	    	// Hilfsklasse Objects neu in JDK 7
	    	Objects.requireNonNull(name, "Name ist erforderlich!");
	    	Objects.requireNonNull(birthday, "Geburtstag ist erforderlich");
	    	Objects.requireNonNull(city, "Stadt muss angegeben werden");
	        
	        this._name = name;
	        this._birthday = birthday;
	        this._city = city;
	    }

	    public final String getName()       { return _name; }
	    public final Date   getBirthday()   { return _birthday; }
	    public final String getCity()       { return _city; }
	   
	       
	    public boolean equals(Object other)
	    {
	        if (other == null)          
	            return false;
	            
	        if (this == other)          
	            return true;
	            
	        if (this.getClass() != other.getClass())   
	            return false;      
	        
	        final Person otherPerson = (Person)other;
	        return compareAttributes(otherPerson);
	    }
	    
	    private boolean compareAttributes(final Person otherPerson)
	    {
	        return this.getName().equals(otherPerson.getName()) &&                
	               this.getBirthday().equals(otherPerson.getBirthday()) &&
	               this.getCity().equals(otherPerson.getCity());           
	    }      
	    
	    @SuppressWarnings("deprecation")
		public final int getAge()
	    {
	        final Date now = new Date();

	        final int monthNow = now.getMonth();
	        final int monthBirthDay = _birthday.getMonth();
	        final int dayNow = now.getDate();
	        final int dayBirthDay = _birthday.getDate();

	        final int correction;
	        if (monthNow > monthBirthDay || 
	           (monthNow == monthBirthDay && dayNow >= dayBirthDay))
	        {
	            correction = 0; // keine Korrektur, wenn Monat und Tag erreicht  
	        }
	        else
	        {
	            correction = -1;
	        }

	        return now.getYear() - _birthday.getYear() + correction;
	    }
	    
	    // Vermeide FindBugs Warning, obwohl diese Klasse niemals in Hash-Container verwendet wird
	    @Override
	    public int hashCode()
	    {
	    	return Objects.hash(this._name, this._birthday, this._city);
	    } 
}
