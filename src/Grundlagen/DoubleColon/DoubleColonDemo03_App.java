package Grundlagen.DoubleColon;

import java.util.ArrayList;
import java.util.List;

public class DoubleColonDemo03_App {

	public static void main(String[] args) {
		  List<Integer> a = new ArrayList<>();
         // var a = new ArrayList<Integer>();
         
          for(int i=0;i < 10;i++){
            a.add((int)(Math.random() * 99 ));
          }
          
          a.forEach(Person::staticMethod);
          a.stream().map(new Person()::instanzMethod).forEach(System.out::println);

	}

}
