package Grundlagen.Interfaces;

import java.util.Arrays;
import java.util.List;

public class Defaultmethoden_App {

	public static void main(String[] args) {
		/**
		 * Beim Entwurf von Lambdas stellte sich heraus, dass bestehende Klassen 
		 * und Interfaces aus dem JDK erweitert werden mussten.
		 * 
		 * Defaultmethoden sort() und forEach()
		 */
		final List<String> names = Arrays.asList("Susi Sorglos", "Rudi der Ratlose", "Fritz", "Anna Bolika");
		System.out.println(names);
        names.sort((str1, str2) -> Integer.compare(str1.length(), str2.length()));
        names.forEach(it -> System.out.print(it.length() + ", "));
        System.out.println(names);
        
        /**
         * Es war vor dem JDK 8 nicht m�glich ein Interface zu erweitern!
         * Jetzt k�nnen mit sogenannten Defaultmethoden spezielle Implemen-
         * tierungen von Methoden vorgenommen werden, die mit "default" 
         * eingeleitet werden! Damit unterscheiden sie sich von den normalen 
         * abstrakten Methoden! 
         */
        //https://docs.oracle.com/javase/8/docs/api/java/lang/Iterable.html#forEach-java.util.function.Consumer-
        
        /**
         * Unter java.util.function
         * existieren ca 40 Funtionale Interfaces, so z.B
         * Consumer<T> -  Methode  void accept(T)
         * Predicate<T>      "     boolean test(T) - > Filterbedingungen
         * Function<T , R> 
         */
	}

}
