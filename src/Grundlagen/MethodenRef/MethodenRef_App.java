package Grundlagen.MethodenRef;

import java.util.Arrays;
import java.util.List;

public class MethodenRef_App {

	public static void main(String[] args) {
		final List<String> names = Arrays.asList("Susi Sorglos", "Rudi der Ratlose", "Fritz", "Anna Bolika");
			
		// Lambda
		names.forEach(it -> System.out.println(it));
		/**
		 * Methodenreferenz kann anstelle eines Lambda zur
		 * Vereinfachung der Schreibweise eingesetzt werden!
		 */
		
		// Methodenreferenz
		names.forEach(System.out::println);
		
		
	}

}
