package Grundlagen.CollectionFuncInterfaces;

import java.util.function.UnaryOperator;

public class UnaryOperator_App {

	public static void main(String[] args) {
		
		/**
		 * Statische Methode identity()
		 * und 
		 * apply(T)
		 */
		 // Marker
        final UnaryOperator<String> markTextWithM = str -> str.startsWith("M") ? ">>" + str.toUpperCase() + "<<" : str;
        printResult("Marker 1", "unchanged", markTextWithM);
        printResult("Marker 2", "Michael", markTextWithM);

        // Trimmer
        final UnaryOperator<String> trimmer = String::trim;
        printResult("Trimmer 1", "no_trim", trimmer);
        printResult("Trimmer 2", " trim me ", trimmer);

        // Mapper
        final UnaryOperator<String> mapNullToEmpty = str -> str == null ? "" : str;
        printResult("Mapper same", "same", mapNullToEmpty);
        printResult("Mapper null", null, mapNullToEmpty);
    }

    private static void printResult(final String text, final String value, final UnaryOperator<String> op)
    {
        System.out.println(text + ": '" + value + "' -> '" + op.apply(value) + "'");
    }

}
