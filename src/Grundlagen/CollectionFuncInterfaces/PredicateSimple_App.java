package Grundlagen.CollectionFuncInterfaces;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.function.Predicate;

import Grundlagen.ClassObject.Person;

public class PredicateSimple_App {

	public static void main(String[] args) throws ParseException {
		// Pr�dikate formulieren
		final Predicate<String> isNull = str -> str == null;
		final Predicate<String> isEmpty = String::isEmpty;
		final Predicate<Person> isAdult = person -> person.getAge() >= 18;
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		

		System.out.println("isNull(''):     " + isNull.test(""));
		System.out.println("isEmpty(''):    " + isEmpty.test(""));
		System.out.println("isEmpty('Anna'): " + isEmpty.test("Anna"));
		System.out.println("isAdult(Anna):   " + isAdult.test(
				new Person("Anna",  dateFormat.parse("4-12-1998"),
						"Jena")));
//https://stackoverflow.com/questions/4216745/java-string-to-date-conversion	
	}

}
