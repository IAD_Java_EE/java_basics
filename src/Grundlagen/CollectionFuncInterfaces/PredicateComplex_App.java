package Grundlagen.CollectionFuncInterfaces;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import Grundlagen.ClassObject.Person;

public class PredicateComplex_App {
	public static void main(String[] args) throws ParseException {
		
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		  final List<Person> persons = new ArrayList<>();
		    persons.add(new Person("Rudi", dateFormat.parse("4-12-1998"),"Erfurt"));
		    persons.add(new Person("Barbara", dateFormat.parse("4-12-1998"),"Gotha"));
		    persons.add(new Person("Toni", dateFormat.parse("4-12-1998"),"Weimar"));
		    persons.add(new Person("Felix", dateFormat.parse("4-12-1998"),"Vieselbach"));
			
			// Einfache Pr�dikate formulieren
			final Predicate<Person> isAdult = person -> person.getAge() >= 18;
				
			// Negation einzelner Pr�dikate
			@SuppressWarnings("unused")
			final Predicate<Person> isYoung = isAdult.negate();
			
		
			
			persons.forEach(System.out::println);
	}
}
