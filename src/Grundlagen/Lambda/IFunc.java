package Grundlagen.Lambda;
@FunctionalInterface
public interface IFunc {
	void foo();
	@Override
	int hashCode();
}