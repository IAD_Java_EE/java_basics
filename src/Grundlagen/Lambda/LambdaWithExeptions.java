package Grundlagen.Lambda;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class LambdaWithExeptions {

	static List<File> collectFiles() {

		return new ArrayList<File>();
	}

	/**
	 * LAmbdas stellen nur ein Stück ausführbaren Code dar! Die Ausführung selbst
	 * kann an einer anderen Stelle geschen. Deshalb ist der Ausführungscode in
	 * einem try Block zu hinterlegen!
	 */
	public static void main(String[] args) throws IOException {
		try {
			@SuppressWarnings("unused")
			final List<File> files = collectFiles();
			@SuppressWarnings("unused")
			final Comparator<File> fileCoparator= createFileCoparator();
			
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	private static Comparator<File> createFileCoparator() {
		final Comparator<File> fileComparator = (file1 , file2)->
		{
			@SuppressWarnings("unused")
			final Path path1 = file1.toPath();
			//tue das Richtige
			return 1;
		};
		return fileComparator;
		
	}

}
