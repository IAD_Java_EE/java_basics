package Grundlagen.Lambda;

public class LambdaSyntax_App {

	public static void main(String[] args) {
		
		/**
		 * Lambdas Syntax
		 * (Parameter- liste) -> {Ausdruck oder Anweisungen}
		 * 
		 * Lambda stellen ein St�ck ausf�hrbaren Code dar!
		 * - sie besitzen keinen Namen sondre n nur Funktionalit�t
		 * - keine explizite Angbe des R�ckgabetypes
		 * - keine Deklaration von Exceptions erlaubt
		 * 
		 * Bis JDK 8 konnnte in Java jede Referenz auf dem Basistyp
		 * Object abgebildet werden.
		 * 
		 * Lambda k�nnen nur einem funktionalen Interfaces mit der entsprechenden 
		 * Signatur.
		 * 
		 * SAM Single Abstract Method - Ein Interface mit genau nur einer Methode!!!
		 * 
		 *  Beispiele sind :
		 *  
		 *  Runnable, Callable<V> , Comparator<T>
		 *  FileFilter, FilenameFilter, ActionListener, Eventhandler usw
		 *  
		 *  Als Besonderheit gilt, das alle im Type Objekt definierten Methoden
		 *  k�nnen zus�tzlich zu der abstrakten Methode angegeben werden!
		 *  
		 *  Grunds�tzlich kann man feststellen, das man mit einem Lambda
		 *  Ausdruck die Trennung von Wie und Was man tut.
		 *  z.B. Ver�nderung einer Liste von Objekten - was
		 *  konkrete �nderung das Wie
		 */
		IFunc fn1 = () ->  System.out.println(1);
        fn1.foo();
        
        IFuncParam fn2 = (int a, int b) ->  System.out.println(a + b);
        fn2.foo(4, 5);
        
        IFuncParamReturn fn3 = (int a, int b) ->  {return a + b;};
        System.out.println(fn3.foo(6, 9));

	}

}
