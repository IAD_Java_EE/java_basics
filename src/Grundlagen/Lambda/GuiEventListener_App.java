package Grundlagen.Lambda;

import javax.swing.JButton;
import javax.swing.JFrame;

public class GuiEventListener_App extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	GuiEventListener_App() {
		this.setSize(400, 200);
		JButton btn = new JButton("Dr�ck mich");
		/*
		 * btn.addActionListener(new ActionListener() {
		 * 
		 * @Override public void actionPerformed(ActionEvent e) { // TODO Auto-generated
		 * method stub System.exit(1);
		 * 
		 * }
		 * 
		 * });
		 */
		btn.addActionListener(e -> {
			System.exit(2);
		});
		this.add(btn);

	}

	public static void main(String[] args) {
			new GuiEventListener_App().setVisible(true);
	}
}
