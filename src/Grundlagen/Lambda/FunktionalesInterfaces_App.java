package Grundlagen.Lambda;

import java.util.Comparator;

public class FunktionalesInterfaces_App {
	public static void main(String[] args) {
		
		@SuppressWarnings("unused")
		Runnable runAsyncClass = new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				
			}
		};
		@SuppressWarnings("unused")
		Runnable runAsyncLambda = ()->System.out.println("runns");
		
		@SuppressWarnings("unused")
		Comparator <String> comparatorByLength = new Comparator<>() {
			@Override
			public int compare(final String str1, String str2) {
				return Integer.compare(str1.length(),str2.length());
			}
			
		};
		
		@SuppressWarnings("unused")
		Comparator<String> compareByLambda = (final String str1,final String str2)->
		{return Integer.compare(str1.length(), str2.length());};
		
		/** Type Inference
		 */
		@SuppressWarnings("unused")
		Comparator<String> compareByLambda2 = (str1,str2)->
		{return Integer.compare(str1.length(), str2.length());};
	}
}
