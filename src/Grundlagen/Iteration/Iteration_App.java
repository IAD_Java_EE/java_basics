package Grundlagen.Iteration;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Iteration_App {

	public static void main(String[] args) {
		
	//http://openbook.rheinwerk-verlag.de/javainsel9/javainsel_13_002.htm
	// Externe Iteration
		final List<String> names = Arrays.asList("Susi Sorglos", "Rudi der Ratlose", "Fritz", "Anna Bolika");
		final Iterator<String> it= names.iterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}
		
		for(int i = 0 ; i< names.size(); i++) {
			System.out.println(names.get(i));
		}

		// Ab JDK 5
		for(final String name: names) {
			System.out.println(name);
		}
		
		// Interne Iteration
		names.forEach(name -> System.out.println(name));
		names.forEach(System.out::println);
		
	}

}
