package Collection.API;

import java.util.List;

public class CollectionDemo_App {

	public static void main(String[] args) {
		//JAVA 8
		/*List<String> lst = new ArrayList<>();
		lst.add("Rudi");
		lst.add("Susi");
		lst.add("Anna");
		lst.add("Toni");
		// Unveränderliche Liste
		lst = Collections.unmodifiableList(lst);
		lst.add("Besen");*/
		//Java 9
		@SuppressWarnings("unused")
		List<String> lst = List.of("Pause");

	}

}
