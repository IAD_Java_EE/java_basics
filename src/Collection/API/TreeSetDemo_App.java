package Collection.API;


import java.util.Comparator;
import java.util.TreeSet;

public class TreeSetDemo_App {

	public static void main(String[] args) {

		var s = new TreeSet<String>();
		s.add("Zitrone");
		s.add("Kirsche");
		s.add("Ananas");
		s.add("Apfel");
		s.add("Grapefruit");
		s.add("Banane");

		// Sortierte Ausgabe
		var it = s.iterator();
		while (it.hasNext()) {
			System.out.println( it.next());
		}

		var sr = new TreeSet<String>(new ReverseStringComparator());
		sr.add("Zitrone");
		sr.add("Kirsche");
		sr.add("Ananas");
		sr.add("Apfel");
		sr.add("Grapefruit");
		sr.add("Banane");

		System.out.println("----------- Ausgabe reverse ---------------");
		it = sr.iterator();
		while (it.hasNext()) {
			System.out.println( it.next());
		}

		
		var  str = new TreeSet<String>(new Comparator<String>() {
	        @Override
	        public int compare(String s1, String s2) {
	            return s1.trim().compareTo(s2.trim());
	        }
	    });
		
		str.add("Zitrone");
		str.add("Kirsche");
		str.add("Ananas");
		str.add("Apfel");
		str.add("Grapefruit");
		str.add("Banane");
		
		Comparator<String> comp = (String o1, String o2) -> (o1.compareTo(o2));
		var ts = new TreeSet<String>(comp);
		
		ts.add("Zitrone");
		ts.add("Kirsche");
		ts.add("Ananas");
		ts.add("Apfel");
		ts.add("Grapefruit");
		ts.add("Banane");
		
		System.out.println("----------- Ausgabe reverse ---------------");
		
		it = ts.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }
	}

}
