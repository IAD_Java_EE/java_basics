package Collection.API;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

public class HashMapDemo_App {

	public static void main(String[] args) {
		var h = new HashMap<String, String>();

		
		h.put("Rudi", "r.ratlos@test.de");
		h.put("Susi", "suso@web.de");
		h.put("Paula", "user0125@mail.iad.edu");
		h.put("Anna", "Anna.Bolika@unheimlich.gut.dk");

		
		var it = h.entrySet().iterator();
		while (it.hasNext()) {
			var entry = (Map.Entry<String, String>) it.next();
			System.out.println((String) entry.getKey() + " --> " + (String) entry.getValue());
		}

		System.out.println("\n ===============\n");
		var ht = new Hashtable<String, String>(h);

		
		var e = ht.keys();
		while (e.hasMoreElements()) {
			String alias = (String) e.nextElement();
			System.out.println(alias + " --> " + ht.get(alias));
		}

	}

}
