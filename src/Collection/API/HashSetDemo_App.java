package Collection.API;

import java.util.HashSet;


public class HashSetDemo_App {

	public static void main(String[] args) {

		var set = new HashSet<>(10);
		int doubletten = 0;
		// Lottozahlen erzeugen
		while (set.size() < 6) {
			int num = (int) (Math.random() * 49) + 1;
			if (!set.add(Integer.valueOf(num))) {
				++doubletten;
			}
		}
		// Lottozahlen ausgeben
		var it = set.iterator();
		while (it.hasNext()) {
			System.out.println(((Integer) it.next()).toString());
		}
		System.out.println("Ignorierte Doubletten: " + doubletten);
	}
}
