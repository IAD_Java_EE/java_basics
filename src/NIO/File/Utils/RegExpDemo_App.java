package NIO.File.Utils;
/**
 * Strings in Java unterst�tzen regul�re Ausdr�cke. 
 * Zeichenfolgen verf�gen �ber vier integrierte Methoden f�r regul�re Ausdr�cke.
 * Die Methoden 
 * matches (), 
 * split (), 
 * replaceFirst () und 
 * replaceAll (). 
 * Die Methode replace () unterst�tzt keine regul�ren Ausdr�cke.
 * 
 * Diese Methoden sind nicht f�r die Leistung optimiert.
 * 
 * @author Susi
 *
 */
public class RegExpDemo_App {
	 public static final String EXAMPLE_TEST = "Das ist mein kleines Beispiel \"\r\n" + 
	 		"+ \"Zeichenkette, die ich\" + \"f�r den Mustervergleich verwenden werde.";

	    public static void main(String[] args) {
	        System.out.println(EXAMPLE_TEST.matches("\\s"));
	        String[] splitString = (EXAMPLE_TEST.split("\\s"));
	        System.out.println(splitString.length);
	        for (String string : splitString) {
	            System.out.println(string);
	        }
	        // Alle Leerzeicehn mit Tab
	        System.out.println(EXAMPLE_TEST.replaceAll("\\s+", "\t"));
	    }
}
