package NIO.File.Utils;

import java.io.File;
import java.io.FilenameFilter;

public class IAD_FileFilter implements FilenameFilter{

	 private final String _vorspann;
	   private final String _typ;
		
	   public IAD_FileFilter(final String vor, final String nach)
	   {
	      this._vorspann = vor;
	      this._typ = nach;
	   }
	
		@Override
		public boolean accept(final File dir, final String name) {
			// TODO Auto-generated method stub
			
			return name.startsWith(_vorspann) && name.endsWith(_typ);
		}
}
