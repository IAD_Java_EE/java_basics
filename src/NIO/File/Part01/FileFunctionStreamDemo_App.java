package NIO.File.Part01;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class FileFunctionStreamDemo_App {
	
	
	
	 public static void main(final String[] args) throws IOException
	    {
	        final String tmpDir = System.getProperty("user.dir");
	        final Path destinationFile = Paths.get(tmpDir + "/Datenquelle.txt");
	       // final List<String> content = Arrays.asList("Das ", "ist ", "mein ", " Inhalt");

	        // Datei schreiben
	       /* final Path resultFile = Files.write(destinationFile, content, StandardOpenOption.CREATE,
	                                            StandardOpenOption.APPEND);
*/
	        // Zeilenweise als Stream<String> einlesen
	        var listContent = Files.readAllLines(destinationFile).spliterator();
	       // final Stream<String> contentAsStream = Files.lines(destinationFile);
	        System.out.println(listContent.SIZED);
	        // Filtern und Gruppieren
	     /*   final Map<Integer, List<String>> filterdAndGrouped = contentAsStream.filter(word -> word.length() > 5)
	                        .collect(Collectors.groupingBy(String::length));
*/
	      //  System.out.println(filterdAndGrouped);

	        // Verzeichnis als Stream<Path> einlesen
	        @SuppressWarnings("resource")
			final Stream<Path> tmpDirContent = Files.list(Paths.get(tmpDir));

	        // Fallstrick: endsWith arbeitet auf Path-Komponenten, nicht auf Dateinamen!
	        tmpDirContent.filter(path -> path.toString().endsWith(".txt")).forEach(System.out::println);
	    }
}
