package NIO.File.Part01;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

//https://stackoverflow.com/questions/14657997/reading-from-file-and-splitting-the-data-in-java
//https://stackoverflow.com/questions/28919559/how-to-read-from-inputfilestream-and-split-each-line
//https://neurosity.info/javastringsplit/
//https://docs.oracle.com/javase/7/docs/api/java/util/regex/Pattern.html
//https://ahkde.github.io/docs/misc/RegEx-QuickRef.htm

public class FileBufferedReadDemo_App {
	static String [] splited;
	static List<String> listResult = new ArrayList<>();
	static Long begin;
	static Long end;
	public static void main(String[] args) {
		final String tmpDir = System.getProperty("user.dir");
		final Path sourceFile = Paths.get(tmpDir + "/DatenQuelle.txt");
		
		try (FileInputStream fileIN = new FileInputStream(sourceFile.toString());
				InputStreamReader inputST = new InputStreamReader(fileIN);
				BufferedReader bufferRe = new BufferedReader(inputST);) {
			String line;
			
			while ((line = bufferRe.readLine()) != null) {
				line = line.replace("!", "");
				line = line.replace("?", "");
				line = line.replace(".", "");
				line = line.replace(",", "");
				splited = line.split("\\s");
				begin = System.currentTimeMillis();
				for (var e : splited) {
					listResult.add(e);
					
				}
				
			}
			
		} catch (IOException e) {
			System.out.println("Nichts gefunden ");
		}
		
		
		
		Set<String> hashSet= new HashSet<>();
		begin = System.nanoTime();
		hashSet.addAll(listResult);
		System.out.println(listResult.size());
		
		end=System.nanoTime();
		System.out.println(hashSet.size());
		
		
		System.out.printf("Erledigt in %.5f ms ", (end -begin)/ 1000000.0);
	}

}
