package NIO.File.Part01;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import NIO.File.Utils.IAD_FileFilter;

public class FileDemo_App {

	public static void main(String[] args) throws IOException {
		// Zugriff auf das systemspezifische tmp-Directory  
	    final String tmpDir = System.getProperty("java.io.tmpdir");
	    
	    // Konstanten f�r die betroffenen Dateien
	    final String SOURCE_PATH = "src/NIO/File/Part01/FileDemo_App.java";
	    final String DESTINATION_PATH1 = tmpDir + "CopiedFile.java";
	    final String DESTINATION_PATH2 = tmpDir + "CopyFileDemo_App.java";

	    // Erzeugen eines Path-Objekts mit der Klasse FileSystem
	    final FileSystem local = FileSystems.getDefault();
	    final Path fromPath = local.getPath(SOURCE_PATH);

	    // Erzeugen von Path-Objekten mit der Utility-Klasse Paths
	    final Path toPath1 = Paths.get(DESTINATION_PATH1);
	    final Path toPath2 = Paths.get(DESTINATION_PATH2);

	    // Dateien vorsorglich l�schen 
	    Files.deleteIfExists(toPath1);
	    Files.deleteIfExists(toPath2);
	    
	    // Kopieren und verschieben 
	    Files.copy(fromPath, toPath1);
	    Files.move(toPath1, toPath2);
	    
	    final File dir = new File(tmpDir);
	    final String[] content = dir.list(new IAD_FileFilter("", ".java"));
	    System.out.println("Inhalt von temp '" + tmpDir + "': " +  Arrays.toString(content));

	}
	
}
