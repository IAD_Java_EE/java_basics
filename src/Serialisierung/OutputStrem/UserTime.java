package Serialisierung.OutputStrem;

import java.io.Serializable;

public class UserTime implements Serializable {

	private static final long serialVersionUID = 1L;
	private int hour;
	private int minute;

	public UserTime(int hour, int minute) {
		this.hour = hour;
		this.minute = minute;
	}

	@Override
	public String toString() {
		return hour + ":" + minute;
	}

}
