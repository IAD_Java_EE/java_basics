package Serialisierung.OutputStrem;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class SerialisierungDemo_App {

	public static void main(String[] args) {

		try (FileOutputStream fs = new FileOutputStream("src\\Serialisierung\\OutputStrem\\test1.ser");
				ObjectOutputStream os = new ObjectOutputStream(fs);) {

			os.writeInt(123);
			os.writeObject("Hallo");
			os.writeObject(new UserTime(10, 30));
			os.writeObject(new UserTime(11, 25));

		} catch (IOException e) {
			System.err.println(e.toString());
		}
		
		//Deserialisierung
		
		try {
			FileInputStream fs = new FileInputStream("src\\Serialisierung\\OutputStrem\\test1.ser");
			ObjectInputStream is = new ObjectInputStream(fs);
			var zahl = is.readInt();
			var info = (String)is.readObject();
			var  time = (UserTime) is.readObject();
			var  time2 = (UserTime) is.readObject();
			System.out.printf("Int %d\ninfo %s \nZeit 1 : %s \nZeit 2 : %s\n",
					zahl, info, time.toString(), time2.toString());
			is.close();
		} catch (ClassNotFoundException e) {
			System.err.println(e.toString());
		} catch (IOException e) {
			System.err.println(e.toString());
	     }
	}

}
